package com.bhinary.unipago;

import android.os.AsyncTask;

import com.bhinary.unipago.interfaces.AsynctaskListener;
import com.bhinary.unipago.interfaces.LoginListener;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

import java.util.Map;

public class Asynctask  {
    private static int timeout = 10000;
    public static final String NOT_FOUND = "NOT-FOUND";
    public static final String RESPONSE_OK = "OK";
    public static final String USER_EXISTS = "User exists";
    public static final String EMAIL_EXISTS = "Email exists";
    public static final String USER_NOT_FOUND = "USER-NOT-FOUND";
    public static final String EMAIL_NOT_FOUND = "EMAIL-NOT-FOUND";
    public static final String ACCOUNT_EXIST = "ACCOUNT-ALREADY-EXISTS";
    public static final String IDENTITY_ERROR = "Pattern.userEntity.identityNumber";
    public static final String PASSWORD_ERROR = "must match \"^(?=.[0-9])(?=.[a-z])(?=.[A-Z])(?=.[_*@#$%^&+=\\.\\-])(?=\\S+$).{8,}$\"";
    public static final String ACCOUNT_NO_EXIST = "BANK-ACCOUNT-NO-EXISTS";
    public static final String MAX_OPERATION_CONSTRAINT = "MAX-OPERATION-CONSTRAINT";
//--------------------------------------------------------------------------------------------------
    private static final String HOST = "http://186.24.254.139:8443";
    //private static final String HOST = "http://172.16.31.1:8443";
    //private static final String HOST = "http://172.16.31.103:8443";
    //private static final String HOST = "http://186.24.254.139:8443";
//--------------------------------------------------------------------------------------------------
    public static final String URL_LOGIN = "/users/login";
    public static final String URL_ADD_USER = "/users/register";
    public static final String URL_ADD_ACCOUNT = "/accounts/register";
    public static final String URL_GET_ACCOUNT = "/accounts/get_accounts";
    public static final String URL_GET_ALL_BANK = "/banks/get_all";
    public static final String URL_RESEND_EMAIL = "/users/resend_email";
    public static final String URL_RESET_PASSWORD = "/users/reset_password";
    public static final String URL_CHANGE_PASSWORD = "/users/change_password";
    public static final String URL_SET_DEFAULT_ACCOUNT = "/accounts/set_default";
    public static final String URL_GET_PAYMENT_RESTRICTION = "/settings/get_default";
    public static final String URL_APPLY_PAYMENT_RESTRICTIONS = "/profile/apply_payments_restrictions";
    public static final String URL_APPLY_NOTIFICATIONS_SETTINGS = "/profile/apply_notifications_settings";
    //--------------------------------------------------------------------------------------------------
    public static final String X_SESSION = "x-session";
    public static final String X_IDENTITY = "x-identity";
//--------------------------------------------------------------------------------------------------

    public static class GetMethodAsynctask extends AsyncTask<String, String, String> {
        private String url, requestHeaders, response;
        private HttpRequest request;
        private Map<String,String> headers;
        private AsynctaskListener listener;

        public GetMethodAsynctask(String url, Map<String, String> headers, AsynctaskListener listener) {
            this.url = url;
            this.headers = headers;
            this.listener = listener;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.get(HOST + url)
                        .accept("application/json")
                        .contentType("application/json")
                        .headers(headers)
                        .connectTimeout(timeout)
                        .readTimeout(timeout)
                        .trustAllHosts()
                        .trustAllCerts();
                response = request.body();
                requestHeaders = new Gson().toJson(request.headers());
            } catch (Exception e) {
                e.getMessage();
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                switch (url) {
                    case URL_GET_ALL_BANK:
                        listener.onAsynctaskFinished(AsynctaskListener.requestType.GETALLBANK,response,requestHeaders);
                        break;
                    case URL_GET_PAYMENT_RESTRICTION:
                        listener.onAsynctaskFinished(AsynctaskListener.requestType.GETPAYMENRESTRICTION, response, requestHeaders);
                        break;
                    case URL_GET_ACCOUNT:
                        listener.onAsynctaskFinished(AsynctaskListener.requestType.GETACCOUNTS,response,requestHeaders);
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    public static class PostMethodAsynctask extends AsyncTask<String, String, String> {
        private String url, response, requestHeaders;
        private HttpRequest request;
        private Map<String, String> headers;
        private String body;
        private AsynctaskListener listener;

        public PostMethodAsynctask(String body,String url, Map<String,String> headers, AsynctaskListener listener) {
            this.url = url;
            this.headers = headers;
            this.listener = listener;
            this.body = body;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.post(HOST + url)
                        .accept("application/json")
                        .headers(headers)
                        .contentType("application/json")
                        .connectTimeout(timeout)
                        .readTimeout(timeout)
                        .send(body)
                        .trustAllHosts()
                        .trustAllCerts();
                response = request.body();
                requestHeaders = new Gson().toJson(request.headers());
            } catch (Exception e) {
                e.getMessage();
                System.out.println("valores : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                System.out.println("valores 2.1 : " + s);
                switch (url) {
                    case URL_ADD_USER:
                        listener.onAsynctaskFinished(AsynctaskListener.requestType.ADDUSER,response, requestHeaders);
                        break;
                    case URL_APPLY_PAYMENT_RESTRICTIONS:
                        listener.onAsynctaskFinished(AsynctaskListener.requestType.APPLYPAYMENTRESTRICTION, response,requestHeaders);
                        break;
                    case URL_ADD_ACCOUNT:
                        listener.onAsynctaskFinished(AsynctaskListener.requestType.ADDACCOUNT, response,requestHeaders);
                        break;
                    case URL_APPLY_NOTIFICATIONS_SETTINGS:
                        listener.onAsynctaskFinished(AsynctaskListener.requestType.APPLYNOTIFICATIONSSETTINGS, response, requestHeaders);
                        break;
                    case URL_SET_DEFAULT_ACCOUNT:
                        listener.onAsynctaskFinished(AsynctaskListener.requestType.SETDEFAUTLACCOUNT, response,requestHeaders);
                        break;
                    case URL_RESET_PASSWORD:
                        listener.onAsynctaskFinished(AsynctaskListener.requestType.RESETPASSWORD,response,requestHeaders);
                        break;
                    case URL_CHANGE_PASSWORD:
                        listener.onAsynctaskFinished(AsynctaskListener.requestType.CHANGEPASS,response,requestHeaders);
                    case URL_RESEND_EMAIL:
                        listener.onAsynctaskFinished(AsynctaskListener.requestType.RESENDEMAIL,response,requestHeaders);
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    public static class PutMethodAsynctask extends AsyncTask<String,String,String> {
        private String url, response, requestHeaders;
        private HttpRequest request;
        private Map<String, String> headers;

        private AsynctaskListener listener;

        public PutMethodAsynctask(String url, Map<String,String> headers, AsynctaskListener listener){
            this.url = url;
            this.headers = headers;
            this.listener = listener;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.put(HOST + url)
                        .accept("application/json")
                        .contentType("application/json")
                        .headers(headers)
                        .connectTimeout(timeout)
                        .readTimeout(timeout);
                response = request.body();
                requestHeaders = new Gson().toJson(request.headers());
            } catch (Exception e) {
                e.getMessage();
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (!s.isEmpty() || !s.equals("")) {
                    //listener.onAsynctaskFinished(s,responsetHeaders);
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    public static class DeleteMethodAsynctask extends AsyncTask<String,String,String> {
        private String url, response, requestHeaders;
        private HttpRequest request;
        private Map<String,String> headers;
        private AsynctaskListener listener;

        public DeleteMethodAsynctask(String url, Map<String,String> headers, AsynctaskListener listener) {
            this.url = url;
            this.headers = headers;
            this.listener = listener;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.delete(HOST + url)
                        .accept("application/json")
                        .contentType("application/json")
                        .headers(headers)
                        .readTimeout(timeout)
                        .connectTimeout(timeout);
                response = request.body();
                requestHeaders = new Gson().toJson(request.headers());
            } catch (Exception e) {
                e.getMessage();
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (!s.isEmpty() || !s.equals("")) {
                    //listener.onAsynctaskFinished(s,responsetHeaders);
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    public static class LoginAsynctask extends AsyncTask<String, String, String> {
        private String url, responseHeaders, response, user, pass;
        private HttpRequest request;
        private LoginListener listener;

        public LoginAsynctask(String url, LoginListener listener, String user, String pass) {
            this.url = url;
            this.listener = listener;
            this.user = user;
            this.pass = pass;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.post(HOST + url)
                        .accept("application/json")
                        .contentType("application/json")
                        .readTimeout(timeout)
                        .connectTimeout(timeout)
                        .basic(user,pass)
                        .trustAllHosts()
                        .trustAllCerts();
                response = request.body();
                responseHeaders = new Gson().toJson(request.headers());
            } catch (Exception e) {
                e.getMessage();
                System.out.println("valores 3 : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                listener.onLoginFinish(s, responseHeaders);
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }
}
