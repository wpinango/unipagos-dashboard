package com.bhinary.unipago.interfaces;

public interface AccountNumberListener {
    void onFourNumberListener();
    void onFourNumberQuitListener();
}
