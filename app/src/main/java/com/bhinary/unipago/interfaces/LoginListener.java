package com.bhinary.unipago.interfaces;

public interface LoginListener {
    void onLoginFinish(String response, String headers);
}
