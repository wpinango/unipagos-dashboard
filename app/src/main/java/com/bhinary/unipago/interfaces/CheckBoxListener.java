package com.bhinary.unipago.interfaces;

public interface CheckBoxListener {
    void onCheckedChangeListener(int pos, boolean b);
}
