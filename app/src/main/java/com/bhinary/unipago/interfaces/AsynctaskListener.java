package com.bhinary.unipago.interfaces;

public interface AsynctaskListener {
    enum requestType {
        REQUESTIDENTITY,
        PROCESSPAYMENT,
        ADDUSER,
        APPLYPAYMENTRESTRICTION,
        GETALLBANK,
        ADDACCOUNT,
        GETPAYMENRESTRICTION,
        APPLYNOTIFICATIONSSETTINGS,
        GETACCOUNTS,
        SETDEFAUTLACCOUNT,
        RESETPASSWORD,
        CHANGEPASS,
        RESENDEMAIL
    }

    void onAsynctaskFinished(Enum requestType, String response, String headers);
}
