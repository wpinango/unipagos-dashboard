package com.bhinary.unipago.watchers;

import android.text.Editable;
import android.text.TextWatcher;

import com.bhinary.unipago.interfaces.AccountNumberListener;

public class AccountNumberWatcher implements TextWatcher {

    private AccountNumberListener listener;


    public AccountNumberWatcher(AccountNumberListener listener) {
        this.listener = listener;

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if(charSequence.length() == 4){
            listener.onFourNumberListener();
        }
        if (charSequence.length() < 4) {
            listener.onFourNumberQuitListener();
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
