package com.bhinary.unipago.common;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class Format {

    public static String getCashFormat(int cash) {
        String pattern = "#,###.00";
        DecimalFormat format = (DecimalFormat) NumberFormat.getNumberInstance(Locale.GERMAN);
        format.applyPattern(pattern);
        if (cash == 0) {
            return "0 Bs.";
        } else {
            return format.format(cash) + " Bs.";
        }
    }

    public static String getFormatBankAccount(String value){
        return value.replaceFirst("(\\d{4}), (\\d{4})", "$1 ");
    }
}
