package com.bhinary.unipago.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.bhinary.unipago.R;
import com.bhinary.unipago.interfaces.CheckBoxListener;
import com.bhinary.unipago.models.BankAccount;
import com.bhinary.unipago.widgets.RTextView;

import java.util.ArrayList;

public class BankAccountAdapter extends BaseAdapter{

    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<BankAccount>bankAccounts;
    private CheckBoxListener listener;

    public BankAccountAdapter(Context context, ArrayList<BankAccount> bankAccounts, CheckBoxListener listener) {
        this.bankAccounts = bankAccounts;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return bankAccounts.size();
    }

    @Override
    public Object getItem(int i) {
        return bankAccounts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list_account, null);
        }
        RTextView tvBankName = view.findViewById(R.id.tv_item_list_bank_name);
        RTextView tvAccountNumber = view.findViewById(R.id.tv_item_list_account_number);
        RTextView tvAccountType = view.findViewById(R.id.tv_item_list_account_type);
        CheckBox cbDefault = view.findViewById(R.id.cb_item_list_default_account);
        try {
            BankAccount bankAccount = (BankAccount) getItem(i);
            tvBankName.setText(bankAccount.getBankName());
            tvAccountNumber.setText(bankAccount.getAccountNumber());
            switch (bankAccount.getDefaultAccount()) {
                case 0:
                    cbDefault.setChecked(false);
                    break;
                case 1:
                    cbDefault.setChecked(true);
                    break;
            }
            switch (bankAccount.getType()){
                case 0:
                    tvAccountType.setText("Ahorro");
                    break;
                case 1:
                    tvAccountType.setText("Corriente");
            }
            if (bankAccount.isSelect()) {
                view.setBackgroundResource(R.color.colorSelectBlueTransparen);
            } else {
                view.setBackgroundResource(R.color.white);
            }
            /*cbDefault.setOnCheckedChangeListener((compoundButton, b) ->
                    listener.onCheckedChangeListener(i, b));*/
            cbDefault.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onCheckedChangeListener(i,true);
                }
            });
        }catch (Exception e) {
            e.getMessage();
        }
        return view;
    }
}
