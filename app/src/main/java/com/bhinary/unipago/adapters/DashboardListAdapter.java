package com.bhinary.unipago.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bhinary.unipago.R;
import com.bhinary.unipago.common.Format;
import com.bhinary.unipago.interfaces.Dashboard;
import com.bhinary.unipago.models.DashboardHeader;
import com.bhinary.unipago.models.DashboardItem;

import java.util.ArrayList;


public class DashboardListAdapter extends BaseAdapter{

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList list;


    public DashboardListAdapter(Context context, ArrayList list) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Dashboard l = (Dashboard) getItem(i);
        if (l.isHeader()){
            if (view == null) {
                view = layoutInflater.inflate(R.layout.header_dashboard, null);
            }
            TextView tvHeaderName = view.findViewById(R.id.tv_dashboard_header_name);
            TextView tvAmount = view.findViewById(R.id.tv_dashboard_header_amount);
            try {
                DashboardHeader dashboardHeader = (DashboardHeader) l;
                tvAmount.setText(Format.getCashFormat(dashboardHeader.getAmount()));
                tvHeaderName.setText(dashboardHeader.getHeaderName());
            } catch (Exception e) {
                e.getMessage();
            }
        } else {
            if (view == null) {
                view = layoutInflater.inflate(R.layout.item_list_dashboard, null);
            }
            TextView tvAmount = view.findViewById(R.id.tv_dashboard_amount);
            TextView tvAccount = view.findViewById(R.id.tv_dashboard_account);
            TextView tvAccountType = view.findViewById(R.id.tv_dashboard_account_type);
            TextView tvAmountType = view.findViewById(R.id.tv_dashboard_amount_type);
            try {
                DashboardItem dashboardItem = (DashboardItem) l;
                tvAmount.setText(Format.getCashFormat(dashboardItem.getAmount()));
                tvAccount.setText(dashboardItem.getAccountName());
                tvAccountType.setText(dashboardItem.getAccountType());
                tvAmountType.setText(dashboardItem.getAmountType());
            }catch (Exception e) {
                e.getMessage();
            }
        }
        return view;
    }
}
