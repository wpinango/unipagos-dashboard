package com.bhinary.unipago.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bhinary.unipago.fragments.RequestPaymentFragment;
import com.bhinary.unipago.fragments.SendPaymentFragment;

public class SectionPagerOperationAdapter extends FragmentPagerAdapter {
    private SendPaymentFragment sendPaymentFragment = new SendPaymentFragment();
    private RequestPaymentFragment requestPaymentFragment = new RequestPaymentFragment();

    public SectionPagerOperationAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return sendPaymentFragment;
            case 1:
                return requestPaymentFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Enviar Pago";
            case 1:
                return "Recibir Pago";
        }
        return null;
    }
}
