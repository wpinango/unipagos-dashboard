package com.bhinary.unipago.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bhinary.unipago.R;
import com.bhinary.unipago.models.Card;
import com.bhinary.unipago.widgets.RTextView;

import java.util.ArrayList;

public class CardListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Card> cards;
    private LayoutInflater layoutInflater;

    public CardListAdapter(Context context, ArrayList<Card> cards) {
        this.cards = cards;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return cards.size();
    }

    @Override
    public Object getItem(int i) {
        return cards.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list_card, null);
        }
        RTextView tvBank = view.findViewById(R.id.tv_item_list_card_bank);
        RTextView tvCardNumber = view.findViewById(R.id.tv_item_list_card_number);
        RTextView tvCardType = view.findViewById(R.id.tv_item_list_card_type);
        ImageView ivCardType = view.findViewById(R.id.iv_item_list_card_type);
        try{
            Card card = (Card) getItem(i);
            tvBank.setText(card.getBank());
            tvCardNumber.setText(card.getCardNumber());
            switch (card.getCardType()){
                case 1:
                    tvCardType.setText("Visa");
                    ivCardType.setImageResource(R.drawable.visa);
                    break;
                case 2:
                    tvCardType.setText("MasterCard");
                    ivCardType.setImageResource(R.drawable.mastercard);
                    break;
            }
            if (card.isSelect()) {
                view.setBackgroundResource(R.color.colorSelectBlueTransparen);
            } else {
                view.setBackgroundResource(R.color.white);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }
}
