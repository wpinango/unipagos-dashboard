package com.bhinary.unipago.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bhinary.unipago.R;
import com.bhinary.unipago.common.Format;
import com.bhinary.unipago.models.Balance;
import com.bhinary.unipago.widgets.RTextView;

import java.util.ArrayList;

public class BalanceListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Balance>balances;
    private LayoutInflater layoutInflater;

    public BalanceListAdapter(Context context, ArrayList<Balance>balances) {
        this.context = context;
        this.balances = balances;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return balances.size();
    }

    @Override
    public Object getItem(int i) {
        return balances.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list_balance,null);
        }
        RTextView tvDescription = view.findViewById(R.id.tv_item_list_description_balance);
        TextView tvDay = view.findViewById(R.id.tv_item_list_day_balance);
        TextView tvMonth = view.findViewById(R.id.tv_item_list_month_balance);
        RTextView tvBalance = view.findViewById(R.id.tv_item_list_balance);
        RTextView tvAmount = view.findViewById(R.id.tv_item_list_transaction_balance);
        RTextView tvReference = view.findViewById(R.id.item_list_ref_balance);
        try {
            Balance balance = (Balance) getItem(i);
            tvBalance.setText(Format.getCashFormat(balance.getBalance()));
            tvDay.setText(balance.getDay());
            tvMonth.setText(balance.getMonth());
            tvDescription.setText(balance.getDescription());
            tvReference.setText(balance.getReference());
            if (balance.isDeposit()) {
                tvAmount.setText(Format.getCashFormat(balance.getAmount()));
            } else {
                tvAmount.setText(String.format("-%s", Format.getCashFormat(balance.getAmount())));
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }
}
