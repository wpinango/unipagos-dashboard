package com.bhinary.unipago.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bhinary.unipago.fragments.AccountFragment;
import com.bhinary.unipago.fragments.NotificationFragment;
import com.bhinary.unipago.fragments.RestrictionFragment;

public class SectionPagerAdapter extends FragmentPagerAdapter {
    private AccountFragment accountFragment = new AccountFragment();
    private NotificationFragment notificationFragment = new NotificationFragment();
    private RestrictionFragment restrictionFragment = new RestrictionFragment();

    public SectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return accountFragment;
            case 1:
                return restrictionFragment;
            case 2:
                return notificationFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Cuenta";
            case 1:
                return "Restricciones";
            case 2:
                return "Notificaciones";
        }
        return null;
    }


    public void accountValidate() {
        if (accountFragment.validate()) {
            accountFragment.populateDataAccount();
        }
    }

    public void restrictionValidate() {
        if(restrictionFragment.validate()){
            restrictionFragment.requestApplyRestrictions();
        }
    }

    public void notificationValidate(){
        if (notificationFragment.validate()){
            notificationFragment.requestNotificationConfig();
        }
    }
}
