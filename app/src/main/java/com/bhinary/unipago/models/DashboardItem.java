package com.bhinary.unipago.models;

import com.bhinary.unipago.interfaces.Dashboard;

public class DashboardItem implements Dashboard {

    private int amount;
    private String accountName;
    private String accountType;
    private String amountType;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    @Override
    public boolean isHeader() {
        return false;
    }
}
