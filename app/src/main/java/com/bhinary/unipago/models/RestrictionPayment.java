package com.bhinary.unipago.models;

public class RestrictionPayment {

    private int id;
    private int maxPaymentWithoutAuth = 0;
    private int maxOverdraftAmount = 0;
    private String createdAt;
    private String updatedAt;

    public int getMaxPaymentWithoutAuth() {
        return maxPaymentWithoutAuth;
    }

    public void setMaxPaymentWithoutAuth(int maxPaymentWithoutAuth) {
        this.maxPaymentWithoutAuth = maxPaymentWithoutAuth;
    }

    public int getMaxOverdraftAmount() {
        return maxOverdraftAmount;
    }

    public void setMaxOverdraftAmount(int maxOverdraftAmount) {
        this.maxOverdraftAmount = maxOverdraftAmount;
    }

    /*"id": 1,
        "maxPaymentWithoutAuth": 1000000,
        "maxOverdraftAmount": 1000000,
        "createdAt": null,
        "updatedAt": null*/
}
