package com.bhinary.unipago.models;

import com.bhinary.unipago.interfaces.Dashboard;

public class DashboardHeader implements Dashboard {
    private String headerName;
    private int amount;

    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean isHeader() {
        return true;
    }
}
