package com.bhinary.unipago.models;

import java.util.ArrayList;

public class Card {

    private int customerId;
    private String cardNumber;
    private int status;
    private String bank;
    private String createdAt;
    private String updatedAt;
    private int cardType;
    private boolean select;

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public int getCardType() {
        return cardType;
    }

    public void setCardType(int cardType) {
        this.cardType = cardType;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    public static Card getCardSelected(ArrayList<Card> cards) {
        for (Card c: cards) {
            if (c.isSelect()) {
                return c;
            }
        }
        return null;
    }

    public static void deselectCardList(ArrayList<Card> cards) {
        for (Card c : cards) {
            c.setSelect(false);
        }
    }
}
