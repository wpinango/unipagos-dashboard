package com.bhinary.unipago.models;

public class AccountStatus {
    public static int EMAIL_NOT_VALIDATED = 0;
    public static int ACCOUNT_NOT_CONFIGURATED = 1;
    public static int EMAIL_AND_ACCOUNT_VALIDATE = 2;
}
