package com.bhinary.unipago.models;

public class Transaction {

    private String reference;
    private String amount;
    private Coordinates coordinates;
    private String destinationAccount;
    private String transmitterAccount;
    private String commerceName;
    private String transmitterBank;
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTransmitterBank() {
        return transmitterBank;
    }

    public void setTransmitterBank(String transmitterBank) {
        this.transmitterBank = transmitterBank;
    }

    public String getCommerceName() {
        return commerceName;
    }

    public void setCommerceName(String commerceName) {
        this.commerceName = commerceName;
    }

    public String getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(String destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    public String getTransmitterAccount() {
        return transmitterAccount;
    }

    public void setTransmitterAccount(String transmitterAccount) {
        this.transmitterAccount = transmitterAccount;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }
}
