package com.bhinary.unipago.models;

import java.util.ArrayList;

public class Response {
    private long timestamp;
    private String error;
    private String message;
    private Object data;
    private ArrayList<FieldErrors> fieldErrors;
    private String other;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ArrayList<FieldErrors> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(ArrayList<FieldErrors> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }
}
