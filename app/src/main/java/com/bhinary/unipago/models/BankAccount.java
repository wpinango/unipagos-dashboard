package com.bhinary.unipago.models;

import java.util.ArrayList;

public class BankAccount {

    private int id;
    private String accountNumber;
    private String bankName;
    private int status;
    private int defaultAccount;
    private int type;
    private boolean isSelect;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDefaultAccount() {
        return defaultAccount;
    }

    public void setDefaultAccount(int defaultAccount) {
        this.defaultAccount = defaultAccount;
    }

    public static BankAccount getBankAccount(ArrayList<BankAccount> bankAccounts) {
        for (BankAccount b: bankAccounts) {
            if (b.isSelect){
                return b;
            }
        }
        return null;
    }

    public static void uncheckedBankAccounts(ArrayList<BankAccount>bankAccounts) {
        for (BankAccount b: bankAccounts) {
            b.setDefaultAccount(0);
        }
    }

    public static void unselectBankAccount(ArrayList<BankAccount> bankAccounts) {
        for (BankAccount b: bankAccounts) {
            b.setSelect(false);
        }
    }
}
