package com.bhinary.unipago.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bhinary.unipago.Asynctask;
import com.bhinary.unipago.Global;
import com.bhinary.unipago.R;
import com.bhinary.unipago.activities.AppConfigurationActivity;
import com.bhinary.unipago.dialog.DialogType;
import com.bhinary.unipago.interfaces.AccountNumberListener;
import com.bhinary.unipago.interfaces.AsynctaskListener;
import com.bhinary.unipago.models.AddBank;
import com.bhinary.unipago.models.Bank;
import com.bhinary.unipago.models.Response;
import com.bhinary.unipago.watchers.AccountNumberWatcher;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AccountFragment extends Fragment implements AsynctaskListener, AccountNumberListener {

    private ArrayList<Bank> banks = new ArrayList<>();
    private EditText etBank, etAccountNumber;
    private RadioButton rbSaving,rbChecking;
    private AddBank addBank;
    private RadioGroup radioGroup;
    private int accountType;
    private DialogType dialogType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_config,null);
        requestBanksList();
        initViews(view);
        return view;
    }

    private void requestBanksList(){
        Map<String,String> headers = new HashMap<>();
        headers.put(Asynctask.X_IDENTITY, Global.userId);
        new Asynctask.GetMethodAsynctask(Asynctask.URL_GET_ALL_BANK, headers,this).execute();
    }

    public void populateDataAccount() {
        getAccountType();
        addBank.setAccountNumber(etAccountNumber.getText().toString());
        addBank.setOwnerId(Integer.valueOf(Global.userId));
        addBank.setStatus(0);
        addBank.setType(accountType);
        registerBankAccount();
    }//status 1 - corriente 2 - ahorro

    private void getAccountType(){
        switch (radioGroup.getCheckedRadioButtonId()){
            case R.id.rb_saving:
                accountType = 2;
                break;
            case R.id.rb_checking:
                accountType = 1;
                break;
        }
    }

    private void registerBankAccount(){
        Map<String, String> map = new HashMap<>();
        map.put(Asynctask.X_IDENTITY,Global.userId);
        dialogType.showProgressDialog("Guardando datos ...");
        System.out.println("valores : " + new Gson().toJson(addBank));
        new Asynctask.PostMethodAsynctask(new Gson().toJson(addBank),Asynctask.URL_ADD_ACCOUNT,map,this).execute();
    }

    @Override
    public void onAsynctaskFinished(Enum requestType, String response, String headers) {
        try {
            dialogType.dismissDialog();
            System.out.println("valores res : " + response);
            if (!response.equals("")) {
                Response res = new Gson().fromJson(response, Response.class);
                if (res.getFieldErrors() == null) {
                    if (requestType.equals(AsynctaskListener.requestType.GETALLBANK)) {
                        if (res.getMessage().equals(Asynctask.RESPONSE_OK)) {
                            Type custom = new TypeToken<ArrayList<Bank>>() {
                            }.getType();
                            banks.addAll((Collection<? extends Bank>) new Gson().fromJson(new Gson().toJson(res.getData()), custom));
                        }
                    }
                    if (requestType.equals(AsynctaskListener.requestType.ADDACCOUNT)) {
                        if (res.getMessage().equals(Asynctask.RESPONSE_OK)) {
                            dialogType.showSuccessDialog(getResources().getString(R.string.success_config));
                            ((AppConfigurationActivity) getActivity()).passToNextFragment();
                        } else if (res.getMessage().equals(Asynctask.ACCOUNT_NO_EXIST)) {
                            dialogType.showErrorDialog(getResources().getString(R.string.error_account_no_exist));
                        } else if (res.getMessage().equals(Asynctask.ACCOUNT_EXIST)) {
                            dialogType.showInfoDialogWithListenerButton(getResources().getString(R.string.info_account_exist),
                                    new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            ((AppConfigurationActivity) getActivity()).passToNextFragment();
                                            dialogType.dismissDialog();
                                        }
                                    });
                        }
                    }
                } else if (res.getFieldErrors() != null) {

                }
            } else {
                dialogType.showDialogSomethingWrong();
            }
        } catch (Exception e){
            e.getMessage();
        }
    }

    private void initViews(View view){
        etAccountNumber = view.findViewById(R.id.et_account_number);
        etBank = view.findViewById(R.id.et_bank);
        rbChecking = view.findViewById(R.id.rb_checking);
        rbSaving = view.findViewById(R.id.rb_saving);
        etAccountNumber.addTextChangedListener(new AccountNumberWatcher(this));
        radioGroup = view.findViewById(R.id.rg_account_type);
        dialogType = new DialogType(getActivity());
        rbChecking.setChecked(true);
    }

    @Override
    public void onFourNumberListener() {
        setBankByAccountNumber();
    }

    @Override
    public void onFourNumberQuitListener() {
        etBank.setText("");
    }

    private String findBankByCode(String bankCode){
        addBank = new AddBank();
        for(Bank b : banks) {
            if (b.getCode().equals(bankCode)){
                addBank.setBankId(b.getId());
                return b.getName();
            }
        }
        return "";
    }

    private void setBankByAccountNumber() {
        etBank.setText(findBankByCode(etAccountNumber.getText().toString().substring(0,4)));
    }

    public boolean validate() {
        boolean valid = true;
        if (etBank.getText().toString().isEmpty() && !etAccountNumber.getText().toString().equals("")){
            setBankByAccountNumber();
        } else if (etBank.getText().toString().isEmpty()) {
            valid = false;
            etBank.setError("Ingrese un numero de cuenta de un banco asociado");
        }  else {
            etBank.setError(null);
        }
        if (etAccountNumber.getText().toString().length() != 20) {
            etAccountNumber.setError("Ingrese un numero de cuenta valido");
            valid = false;
        } else {
            etAccountNumber.setError(null);
        }
        if (!rbSaving.isChecked() && !rbChecking.isChecked()){
            Toast.makeText(getActivity(),getResources().getString(R.string.checked_account_type),Toast.LENGTH_SHORT).show();
            valid = false;
        }
        if (banks.isEmpty()){
            valid = false;
            dialogType.showErrorDialogWithListenerButton(getResources().getString(R.string.error_something_wrong), new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    requestBanksList();
                    dialogType.dismissDialog();
                }
            });
        }
        return valid;
    }
}
