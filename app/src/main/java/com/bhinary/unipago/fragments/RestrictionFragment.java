package com.bhinary.unipago.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bhinary.unipago.Asynctask;
import com.bhinary.unipago.Global;
import com.bhinary.unipago.R;
import com.bhinary.unipago.activities.AppConfigurationActivity;
import com.bhinary.unipago.common.Format;
import com.bhinary.unipago.dialog.DialogType;
import com.bhinary.unipago.interfaces.AsynctaskListener;
import com.bhinary.unipago.models.Response;
import com.bhinary.unipago.models.RestrictionPayment;
import com.bhinary.unipago.watchers.NumberTextWatcherForThousand;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RestrictionFragment extends Fragment implements AsynctaskListener{

    private EditText etMaxAmountWithoutPass, etMaxOperationWithoutPass, etMaxAmountOverdraft;
    private JsonObject jsonObject;
    private int maxPaymentWithoutAuth, maxOperationsWithoutAuth, maxOverdraftAmount;
    private DialogType dialogType;
    private RestrictionPayment restrictionPayment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restriction_config,null);
        initView(view);
        requestPaymentRestrictions();
        return view;
    }

    private void initView(View view){
        etMaxAmountOverdraft = view.findViewById(R.id.et_max_amount_overdraft);
        etMaxAmountWithoutPass = view.findViewById(R.id.et_max_amount_without_pass);
        etMaxOperationWithoutPass = view.findViewById(R.id.et_max_operation_without_pass);
        etMaxAmountWithoutPass.addTextChangedListener(new NumberTextWatcherForThousand(etMaxAmountWithoutPass));
        etMaxAmountOverdraft.addTextChangedListener(new NumberTextWatcherForThousand(etMaxAmountOverdraft));
        dialogType = new DialogType(getActivity());
    }

    private void requestPaymentRestrictions() {
        Map<String, String>headers = new HashMap<>();
        headers.put(Asynctask.X_SESSION, Global.token);
        new Asynctask.GetMethodAsynctask(Asynctask.URL_GET_PAYMENT_RESTRICTION,headers,this).execute();
    }

    public void requestApplyRestrictions() {
        populateRestrictionData();
        Map<String, String> headers = new HashMap<>();
        headers.put(Asynctask.X_IDENTITY,Global.userId);
        dialogType.showProgressDialog(getResources().getString(R.string.dialog_configuring));
        new Asynctask.PostMethodAsynctask(jsonObject.toString(),Asynctask.URL_APPLY_PAYMENT_RESTRICTIONS,headers,this).execute();
    }

    private void populateRestrictionData() {
        prepareData();
        jsonObject = new JsonObject();
        jsonObject.addProperty("userId", Global.userId);
        jsonObject.addProperty("maxPaymentWithoutAuth", maxPaymentWithoutAuth);
        jsonObject.addProperty("maxOperationsWithoutAuth",maxOperationsWithoutAuth);
        jsonObject.addProperty("maxOverdraftAmount",maxOverdraftAmount);
    }

    private void prepareData() {
        maxOverdraftAmount = getMaxOverdraftAmount();
        maxPaymentWithoutAuth = getMaxPaymentWithoutAuth();
        maxOperationsWithoutAuth = Integer.valueOf(etMaxOperationWithoutPass.getText().toString());
    }

    @Override
    public void onAsynctaskFinished(Enum requestType, String response, String headers) {
        try {
            dialogType.dismissDialog();
            System.out.println("valores res : " + response);
            if (!response.equals("")) {
                Response res = new Gson().fromJson(response, Response.class);
                if (res.getFieldErrors() == null) {
                    if (requestType.equals(AsynctaskListener.requestType.GETPAYMENRESTRICTION)) {
                        if (res.getMessage().equals(Asynctask.RESPONSE_OK)) {
                            restrictionPayment = new Gson().fromJson(new Gson().toJson(res.getData()), RestrictionPayment.class);
                        }
                    } else if (requestType.equals(AsynctaskListener.requestType.APPLYPAYMENTRESTRICTION)) {
                        if (res.getMessage().equals(Asynctask.RESPONSE_OK)) {
                            dialogType.showSuccessDialogWithListenerButton(getResources().getString(R.string.success_config),
                                    sweetAlertDialog -> {
                                        ((AppConfigurationActivity) getActivity()).passToNextFragment();
                                        dialogType.dismissDialog();
                                    });
                        } else if (res.getMessage().equals(Asynctask.MAX_OPERATION_CONSTRAINT)) {
                            dialogType.showErrorDialog("Excede el maximo de operaciones, debe ser 30");
                        }
                    }
                } else if (res.getFieldErrors() != null) {

                }
            } else {
                dialogType.showDialogSomethingWrong();
            }
        } catch (Exception e){
            e.getMessage();
        }
    }

    public boolean validate() {
        boolean valid = true;
        if (etMaxOperationWithoutPass.getText().toString().isEmpty()) {
            etMaxOperationWithoutPass.setError(getResources().getString(R.string.error_valid_value));
            valid = false;
        } else if (getMaxOperationsWithoutAuth() > 30) {
            etMaxOperationWithoutPass.setError(getResources().getString(R.string.error_decrese_operation));
        } else {
            etMaxAmountWithoutPass.setError(null);
        }
        if (etMaxAmountWithoutPass.getText().toString().isEmpty()) {
            etMaxAmountWithoutPass.setError(getResources().getString(R.string.error_valid_amount));
            valid = false;
        } else if (getMaxPaymentWithoutAuth() > restrictionPayment.getMaxPaymentWithoutAuth()) {
            etMaxAmountWithoutPass.setError(getResources().getString(R.string.error_decrese_amount) +
                    Format.getCashFormat(restrictionPayment.getMaxPaymentWithoutAuth()));
            valid = false;
        } else {
            etMaxAmountWithoutPass.setError(null);
        }
        if (etMaxAmountOverdraft.getText().toString().isEmpty()) {
            etMaxAmountOverdraft.setError(getResources().getString(R.string.error_valid_amount));
            valid = false;
        } else if (getMaxOverdraftAmount() > restrictionPayment.getMaxOverdraftAmount()) {
            etMaxAmountOverdraft.setError(getResources().getString(R.string.error_decrese_amount) +
                    Format.getCashFormat(restrictionPayment.getMaxPaymentWithoutAuth()));
            valid = false;
        } else {
            etMaxAmountOverdraft.setError(null);
        }
        if (restrictionPayment == null){
            valid = false;
            dialogType.showErrorDialogWithListenerButton(getResources().getString(R.string.error_something_wrong), new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    requestPaymentRestrictions();
                    dialogType.dismissDialog();
                }
            });
        }
        return valid;
    }

    private int getMaxPaymentWithoutAuth(){
        if (!etMaxAmountWithoutPass.getText().toString().equals("")) {
            return Integer.valueOf(etMaxAmountWithoutPass.getText().toString().replace(".", "").replace(",",""));
        } else {
            return 0;
        }
    }

    private int getMaxOverdraftAmount(){
        if (!etMaxAmountOverdraft.getText().toString().equals("")) {
            return Integer.valueOf(etMaxAmountOverdraft.getText().toString().replace(".","").replace(",",""));
        } else {
            return 0;
        }
    }

    private int getMaxOperationsWithoutAuth(){
        if (!etMaxOperationWithoutPass.getText().toString().equals("")){
            return Integer.valueOf(etMaxOperationWithoutPass.getText().toString());
        } else {
            return 0;
        }
    }
}
