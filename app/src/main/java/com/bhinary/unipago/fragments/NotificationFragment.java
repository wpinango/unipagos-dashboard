package com.bhinary.unipago.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bhinary.unipago.Asynctask;
import com.bhinary.unipago.Global;
import com.bhinary.unipago.R;
import com.bhinary.unipago.activities.MainActivity;
import com.bhinary.unipago.dialog.DialogType;
import com.bhinary.unipago.interfaces.AsynctaskListener;
import com.bhinary.unipago.models.Response;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class NotificationFragment extends Fragment implements AsynctaskListener {

    private RadioGroup rgNotification;
    private RadioButton rbLow,rbHigh,rbMedium;
    private JsonObject jsonObject;
    private int level;
    private DialogType dialogType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification_config,null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        rgNotification = view.findViewById(R.id.rg_notification);
        rbMedium = view.findViewById(R.id.rb_medium);
        rbHigh = view.findViewById(R.id.rb_high);
        rbLow = view.findViewById(R.id.rb_low);
        dialogType = new DialogType(getActivity());
    }

    public void requestNotificationConfig(){
        populateNotificationConfigData();
        Map<String,String> headers = new HashMap<>();
        headers.put(Asynctask.X_IDENTITY,Global.userId);
        dialogType.showProgressDialog(getResources().getString(R.string.dialog_configuring));
        new Asynctask.PostMethodAsynctask(jsonObject.toString(),
                Asynctask.URL_APPLY_NOTIFICATIONS_SETTINGS,headers,this).execute();
    }

    private void populateNotificationConfigData(){
        prepareNotificationConfigData();
        jsonObject = new JsonObject();
        jsonObject.addProperty("userId", Global.userId);
        jsonObject.addProperty("level",level);
    }

    private void prepareNotificationConfigData(){
        switch (rgNotification.getCheckedRadioButtonId()){
            case R.id.rb_high:
                level = 1;
                break;
            case R.id.rb_low:
                level = 3;
                break;
            case R.id.rb_medium:
                level = 2;
                break;
        }
    }

    public boolean validate(){
        boolean valid = true;
        if (!rbMedium.isChecked() && !rbLow.isChecked() && !rbHigh.isChecked()){
            Toast.makeText(getActivity(),getResources().getString(R.string.validate_select_notification_setting),Toast.LENGTH_SHORT).show();
            valid = false;
        }
        return valid;
    }

    @Override
    public void onAsynctaskFinished(Enum requestType, String response, String headers) {
        try {
            dialogType.dismissDialog();
            System.out.println("valores res 1 : " + response);
            if (!response.equals("")) {
                Response res = new Gson().fromJson(response, Response.class);
                if (res.getFieldErrors() == null) {
                    if (requestType.equals(AsynctaskListener.requestType.APPLYNOTIFICATIONSSETTINGS)) {
                        if (res.getMessage().equals(Asynctask.RESPONSE_OK)) {
                            dialogType.showSuccessDialogWithListenerButton(getResources().getString(R.string.success_config),
                                    new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            dialogType.dismissDialog();
                                            Intent intent = new Intent(getActivity(), MainActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }
                                    });
                        }
                    }
                } else if (res.getFieldErrors() != null) {

                }
            } else {
                dialogType.showDialogSomethingWrong();
            }
        } catch (Exception e){
            e.getMessage();
        }
    }
}
