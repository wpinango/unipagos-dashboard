package com.bhinary.unipago.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.bhinary.unipago.Asynctask;
import com.bhinary.unipago.Global;
import com.bhinary.unipago.R;
import com.bhinary.unipago.common.PasswordValidator;
import com.bhinary.unipago.dialog.DialogType;
import com.bhinary.unipago.interfaces.AsynctaskListener;
import com.bhinary.unipago.models.Response;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import jp.wasabeef.blurry.Blurry;

public class ChangePassActivity extends AppCompatActivity implements AsynctaskListener{

    private EditText etPass, etRepeatPass;
    private DialogType dialogType;
    private Button btnChangePass;
    private String passCreationHelp = "El password debe contener 8 caracteres, un caracter especial, una letra Mayuscula, minuscula" +
            " y numeros";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        final View view = findViewById(R.id.change_pass_layout);
        view.post(() -> Blurry.with(ChangePassActivity.this).radius(15).sampling(2)
                .onto((ViewGroup) view));
        dialogType = new DialogType(this);
        etPass = findViewById(R.id.et_pass_change);
        etRepeatPass = findViewById(R.id.et_repeat_pass_change);
        btnChangePass = findViewById(R.id.btn_change_password);
        btnChangePass.setOnClickListener(view1 -> {
            if (validate()) {
                requestChangePass();
            }
        });
    }

    private boolean validate() {
        boolean valid = true;
        if (new PasswordValidator().validate(etPass.getText().toString())){
            etPass.setError(null);
        } else {
            valid = false;
            etPass.setError(passCreationHelp);
        }
        if (etRepeatPass.getText().toString().isEmpty()) {
            valid = false;
            etRepeatPass.setError("Ingrese contaseña valida");
        } else {
            etRepeatPass.setError(null);
        }
        if (!etRepeatPass.getText().toString().equals(etPass.getText().toString())){
            valid = false;
            etRepeatPass.setError("No coincide la contraseña");
        } else {
            etRepeatPass.setError(null);
        }
        return valid;
    }

    private void requestChangePass(){
        btnChangePass.setEnabled(false);
        Map<String, String> headers = new HashMap<>();
        headers.put(Asynctask.X_IDENTITY, Global.userId);
        headers.put(Asynctask.X_SESSION,Global.token);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("password",etPass.getText().toString());
        jsonObject.addProperty("confirmPassword", etRepeatPass.getText().toString());
        dialogType.showProgressDialog("Procesando ... ");
        new Asynctask.PostMethodAsynctask(jsonObject.toString(),Asynctask.URL_CHANGE_PASSWORD,headers,
                this).execute();
    }

    @Override
    public void onAsynctaskFinished(Enum requestType, String response, String headers) {
        System.out.println("valores " + response );
        dialogType.dismissDialog();
        btnChangePass.setEnabled(true);
        if (!response.equals("")) {
            Response res = new Gson().fromJson(response,Response.class);
            if (res.getMessage().equals(Asynctask.RESPONSE_OK)) {
                dialogType.showSuccessDialog("Se cambio correctamente la contraseña");
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                this.finish();
            } else if (res.getMessage().equals(Asynctask.USER_NOT_FOUND)) {
                dialogType.showErrorDialog("Usuario no encontrado");
            }
        } else {
            dialogType.showDialogSomethingWrong();
        }
    }
}
