package com.bhinary.unipago.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.bhinary.unipago.R;
import com.bhinary.unipago.adapters.SectionPagerAdapter;
import com.bhinary.unipago.adapters.SectionPagerOperationAdapter;

public class OperationsActivity extends AppCompatActivity{

    private SectionPagerOperationAdapter sectionPagerOperationAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operation);
        tabLayout = findViewById(R.id.tl_operation);
        viewPager = findViewById(R.id.vp_operation);
        sectionPagerOperationAdapter = new SectionPagerOperationAdapter(getSupportFragmentManager());
        viewPager.setAdapter(sectionPagerOperationAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
