package com.bhinary.unipago.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ListView;

import com.bhinary.unipago.R;
import com.bhinary.unipago.adapters.CardListAdapter;
import com.bhinary.unipago.dialog.DialogType;
import com.bhinary.unipago.models.Card;

import java.util.ArrayList;

public class CardActivity extends AppCompatActivity {

    private CardListAdapter cardListAdapter;
    private ArrayList<Card> cards = new ArrayList<>();
    private DialogType dialogType;
    private FloatingActionButton btnAdd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);
        ListView lvCards = findViewById(R.id.lv_cards);
        dialogType = new DialogType(this);
        populateCardList(cards);
        cardListAdapter = new CardListAdapter(this,cards);
        btnAdd = findViewById(R.id.btn_add_card);
        lvCards.setAdapter(cardListAdapter);
        lvCards.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        lvCards.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {
                for(int j = 0; j < cards.size(); j++) {
                    if (i == j) {
                        cards.get(j).setSelect(!cards.get(j).isSelect());
                        cardListAdapter.notifyDataSetChanged();
                    } else {
                        cards.get(j).setSelect(false);
                    }
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                MenuInflater menuInflater = actionMode.getMenuInflater();
                menuInflater.inflate(R.menu.menu_edit_delete,menu);
                btnAdd.setEnabled(false);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menu_delete:
                        dialogType.showWarningDeletetionWithListenerButton("Desea eliminar esta tarjeta?",
                                sweetAlertDialog -> {
                                    dialogType.dismissDialog();
                                    actionMode.finish();
                                    dialogType.showSuccessDialog("Se elimino correctamente");
                                });
                        break;
                    case R.id.menu_edit:
                        dialogType.showInfoDialogWithListenerButton("Desea editar esta tarjeta?",
                                sweetAlertDialog -> {
                                   /* accountDialog(true, BankAccount.getBankAccount(bankAccounts));
                                    dialogType.dismissDialog();
                                    actionMode.finish();*/
                                });
                        break;
                }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
                btnAdd.setEnabled(true);
                Card.deselectCardList(cards);
                cardListAdapter.notifyDataSetChanged();
            }
        });
    }

    private void populateCardList(ArrayList<Card> cards) {
        Card card = new Card();
        card.setCardNumber("1234 5678 1111 2222");
        card.setCardType(1);
        card.setBank("Banco Provincial");
        cards.add(card);
        card = new Card();
        card.setBank("Banco de Venezuela");
        card.setCardType(2);
        card.setCardNumber("2346 9821 2626 8753");
        cards.add(card);
    }
}
