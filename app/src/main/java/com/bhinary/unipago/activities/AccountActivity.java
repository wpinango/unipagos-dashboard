package com.bhinary.unipago.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;

import com.bhinary.unipago.Asynctask;
import com.bhinary.unipago.Global;
import com.bhinary.unipago.R;
import com.bhinary.unipago.adapters.BankAccountAdapter;
import com.bhinary.unipago.dialog.DialogType;
import com.bhinary.unipago.interfaces.AsynctaskListener;
import com.bhinary.unipago.interfaces.CheckBoxListener;
import com.bhinary.unipago.models.BankAccount;
import com.bhinary.unipago.models.Response;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AccountActivity extends AppCompatActivity implements AsynctaskListener, CheckBoxListener {

    private ArrayList<BankAccount> bankAccounts = new ArrayList<>();
    private BankAccountAdapter bankAccountAdapter;
    private DialogType dialogType;
    private FloatingActionButton btnAddAccount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setSubtitle("Cuentas");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        requestAccountList();
        dialogType = new DialogType(this);
        ListView lvAccount = findViewById(R.id.lv_account);
        bankAccountAdapter = new BankAccountAdapter(this,bankAccounts, this);
        View header = getLayoutInflater().inflate(R.layout.item_header_account, null);
        lvAccount.addHeaderView(header);
        lvAccount.setAdapter(bankAccountAdapter);
        lvAccount.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        lvAccount.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {
                for(int j = 1; j < bankAccounts.size() + 1; j++) {
                    if (i == j) {
                        bankAccounts.get(j-1).setSelect(!bankAccounts.get(j-1).isSelect());
                        bankAccountAdapter.notifyDataSetChanged();
                    } else {
                        bankAccounts.get(j-1).setSelect(false);
                    }
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                MenuInflater menuInflater = actionMode.getMenuInflater();
                menuInflater.inflate(R.menu.menu_edit_delete,menu);
                btnAddAccount.setEnabled(false);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menu_delete:
                        dialogType.showWarningDeletetionWithListenerButton("Desea eliminar esta cuenta?",
                                sweetAlertDialog -> {
                            dialogType.dismissDialog();
                            actionMode.finish();
                            dialogType.showSuccessDialog("Se elimino correctamente");
                                });
                        break;
                    case R.id.menu_edit:
                        dialogType.showInfoDialogWithListenerButton("Desea editar esta cuenta?",
                                sweetAlertDialog -> {
                            accountDialog(true, BankAccount.getBankAccount(bankAccounts));
                            dialogType.dismissDialog();
                            actionMode.finish();
                        });
                        break;
                }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
                btnAddAccount.setEnabled(true);
                BankAccount.unselectBankAccount(bankAccounts);
                bankAccountAdapter.notifyDataSetChanged();
            }
        });
        btnAddAccount = findViewById(R.id.btn_add_account);
        btnAddAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accountDialog(false, null);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void requestAccountList(){
        Map<String, String> headers = new HashMap<>();
        headers.put(Asynctask.X_SESSION, Global.token);
        headers.put(Asynctask.X_IDENTITY,Global.userId);
        new Asynctask.GetMethodAsynctask(Asynctask.URL_GET_ACCOUNT,headers,this).execute();
    }

    @Override
    public void onAsynctaskFinished(Enum requestType, String response, String headers) {
        System.out.println("valores : " + response);
        dialogType.dismissDialog();
        if (!response.equals("")) {
            Response res = new Gson().fromJson(response, Response.class);
            if (res.getFieldErrors() == null) {
                if (requestType.equals(AsynctaskListener.requestType.GETACCOUNTS)) {
                    Type custom = new TypeToken<ArrayList<BankAccount>>() {
                    }.getType();
                    ArrayList<BankAccount> b = new Gson().fromJson(new Gson().toJson(res.getData()),custom);
                    bankAccounts.addAll(b);
                    bankAccountAdapter.notifyDataSetChanged();
                }
                if (requestType.equals(AsynctaskListener.requestType.SETDEFAUTLACCOUNT)) {
                    if (!res.getMessage().equals(Asynctask.RESPONSE_OK)) {
                        BankAccount.uncheckedBankAccounts(bankAccounts);
                        bankAccountAdapter.notifyDataSetChanged();
                    }
                }
            } else {
                dialogType.showDialogSomethingWrong();
            }
        } else {
            dialogType.showDialogSomethingWrong();
        }
    }

    @Override
    public void onCheckedChangeListener(int pos, boolean b) {
        dialogType.showInfoDialogWithListenerButtons("Seleccionar como principal?",
                sweetAlertDialog -> {
                    dialogType.dismissDialog();
                    setDefaultAccount(bankAccounts.get(pos));
                },
                sweetAlertDialog -> {
                    dialogType.dismissDialog();
                    BankAccount.uncheckedBankAccounts(bankAccounts);
                    bankAccountAdapter.notifyDataSetChanged();
        });
    }

    private void accountDialog(boolean isEdit, BankAccount bankAccount) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_account_config, null);
        EditText etAccountNumber = view.findViewById(R.id.et_account_number);
        EditText etBank = view.findViewById(R.id.et_bank);
        RadioGroup radioGroup = view.findViewById(R.id.rg_account_type);
        if (isEdit) {
            etAccountNumber.setText(bankAccount.getAccountNumber());
            etBank.setText(bankAccount.getBankName());
        }
        builder.setView(view);
        builder.setPositiveButton("Aceptar", (dialog, which) -> {});
        builder.setNegativeButton("Cancelar",null);
        builder.setCancelable(false);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {

        });
    }

    private void setDefaultAccount(BankAccount bankAccount){
        Map<String, String> headers = new HashMap<>();
        headers.put(Asynctask.X_IDENTITY, Global.userId);
        headers.put(Asynctask.X_SESSION, Global.token);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId",Global.userId);
        jsonObject.addProperty("accountId",bankAccount.getId());
        new Asynctask.PostMethodAsynctask(jsonObject.toString(),Asynctask.URL_SET_DEFAULT_ACCOUNT,
                headers,this).execute();
        dialogType.showProgressDialog("Procesando");
    }

}
