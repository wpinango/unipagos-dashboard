package com.bhinary.unipago.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.bhinary.unipago.R;

public class NotConnectionActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_not_internet);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setSubtitle("Error de comunicacion");
        Button btnConnection = findViewById(R.id.btn_connect);
        btnConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NotConnectionActivity.this,SplashActivity.class);
                startActivity(intent);
                NotConnectionActivity.this.finish();
            }
        });
    }
}
