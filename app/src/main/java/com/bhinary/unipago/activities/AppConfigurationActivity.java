package com.bhinary.unipago.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.bhinary.unipago.R;
import com.bhinary.unipago.adapters.SectionPagerAdapter;
import com.bhinary.unipago.util.NonSwipeableViewPager;

public class AppConfigurationActivity extends AppCompatActivity {
    private NonSwipeableViewPager viewPager;
    private SectionPagerAdapter sectionPagerAdapter;
    private TabLayout tabLayout;
    private Button btnNext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        viewPager = new NonSwipeableViewPager(this);
        viewPager = findViewById(R.id.view_pager);
        sectionPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(sectionPagerAdapter);
        tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        LinearLayout tabStrip = ((LinearLayout)tabLayout.getChildAt(0));
        tabStrip.setEnabled(false);
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setClickable(false);
        }
        btnNext = findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });
    }

    private void validate() {
        if (viewPager.getCurrentItem() == 0) {
            sectionPagerAdapter.accountValidate();
        } else if (viewPager.getCurrentItem() == 1) {
            sectionPagerAdapter.restrictionValidate();
        } else if (viewPager.getCurrentItem() == 2) {
            sectionPagerAdapter.notificationValidate();
        }
    }

    public void passToNextFragment(){
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
    }


}
