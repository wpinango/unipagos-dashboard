package com.bhinary.unipago.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bhinary.unipago.R;
import com.bhinary.unipago.adapters.BalanceListAdapter;
import com.bhinary.unipago.models.Balance;
import com.bhinary.unipago.models.Coordinates;
import com.bhinary.unipago.models.Transaction;
import com.google.gson.Gson;

import java.util.ArrayList;

public class BalanceActivity extends AppCompatActivity {

    private ArrayList<Transaction> transactions = new ArrayList<>();
    private ArrayList<Balance> balances = new ArrayList<>();
    private  BalanceListAdapter balanceListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance);
        ListView lvBalance = findViewById(R.id.lv_balance);
        populateList(balances);
        balanceListAdapter = new BalanceListAdapter(this,balances);
        lvBalance.setAdapter(balanceListAdapter);
        lvBalance.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Coordinates coordinates = new Coordinates();
                coordinates.setLatitude(Float.valueOf(balances.get(i).getLocation().split(" ")[0].split(",")[0]));
                coordinates.setLongitude(Float.valueOf(balances.get(i).getLocation().split(" ")[1]));
                Intent intent = new Intent(BalanceActivity.this,MapViewActivity.class);
                intent.putExtra("longitude",coordinates.getLongitude());
                intent.putExtra("latitude",coordinates.getLatitude());
                intent.putExtra("balance",new Gson().toJson(balances.get(i)));
                startActivity(intent);
            }
        });
    }

    private void populateList(ArrayList<Balance> balances) {
        Balance balance = new Balance();
        balance.setAmount(2500000);
        balance.setDay("25");
        balance.setMonth("JUN");
        balance.setBalance(14500000);
        balance.setDescription("Pago Movil");
        balance.setReference("Ref. 75312");
        balance.setDeposit(true);
        balance.setLocation("9.786316, -63.199497");
        balances.add(balance);
        balance = new Balance();
        balance.setReference("Ref. 15912");
        balance.setAmount(1500000);
        balance.setDescription("Transferencia O/Bancos");
        balance.setBalance(13000000);
        balance.setMonth("JUN");
        balance.setDay("24");
        balance.setDeposit(true);
        balance.setLocation("9.747098, -63.183494");
        balances.add(balance);
        balance = new Balance();
        balance.setReference("Ref. 67415");
        balance.setAmount(3000000);
        balance.setDescription("Transferencia M/Bancos");
        balance.setBalance(13000000);
        balance.setMonth("JUN");
        balance.setDay("23");
        balance.setDeposit(false);
        balance.setLocation("9.736530, -63.218027");
        balances.add(balance);

        /*Transaction transaction = new Transaction();
        transaction.setAmount("500000");
        transaction.setTransmitterBank("Mercantil");
        transaction.setTransmitterAccount("0105 0001 0002 0003");
        transaction.setDestinationAccount("0105 0002 0003 0004");
        transaction.setReference("1030558631");
        transaction.setCommerceName("El caserito");
        Coordinates coordinates = new Coordinates();
        coordinates.setLatitude(9.740037f);
        coordinates.setLongitude(-63.168317f);
        transaction.setCoordinates(coordinates);
        transactions.add(transaction);

        transaction = new Transaction();
        transaction.setAmount("2800000");
        transaction.setTransmitterAccount("0108 0001 0002 0003");
        transaction.setDestinationAccount("0105 0002 0003 0004");
        transaction.setReference("1836508430");
        transaction.setTransmitterBank("Banesco");
        transaction.setCommerceName("Pasteleria Monagas");
        coordinates = new Coordinates();
        coordinates.setLatitude(9.743003f);
        coordinates.setLongitude(-63.167029f);
        transaction.setCoordinates(coordinates);
        transactions.add(transaction);*/


    }
}
