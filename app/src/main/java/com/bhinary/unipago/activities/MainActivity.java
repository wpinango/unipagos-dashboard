package com.bhinary.unipago.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bhinary.unipago.R;
import com.bhinary.unipago.adapters.DashboardListAdapter;
import com.bhinary.unipago.models.DashboardHeader;
import com.bhinary.unipago.models.DashboardItem;
import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList list = new ArrayList();
    private DashboardListAdapter dashboardListAdapter;
    private FloatingActionButton btnAccount, btnCard, btnTransfer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/

        btnTransfer = findViewById(R.id.btn_transfer);
        btnAccount = findViewById(R.id.btn_account);
        btnCard = findViewById(R.id.btn_card);

        btnTransfer.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, OperationsActivity.class);
            startActivity(intent);
        });

        btnAccount.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this,AccountActivity.class);
            startActivity(intent);
        });

        btnCard.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, CardActivity.class);
            startActivity(intent);
        });

        dashboardListAdapter = new DashboardListAdapter(this,list);
        ListView rvDashboard = findViewById(R.id.rv_dashboard);
        rvDashboard.setAdapter(dashboardListAdapter);
        populateArraylist();
        rvDashboard.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 3) {
                    Intent intent = new Intent(MainActivity.this,BalanceActivity.class);
                    startActivity(intent);
                } else if (i < 3) {

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*@SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*/

    private void populateArraylist(){
        DashboardHeader dashboardHeader = new DashboardHeader();
        dashboardHeader.setAmount(14500000);
        dashboardHeader.setHeaderName("Cuentas");
        list.add(dashboardHeader);
        DashboardItem dashboardItem = new DashboardItem();
        dashboardItem.setAccountName("Unibanco *5467");
        dashboardItem.setAmount(10000000);
        dashboardItem.setAccountType("Ahorro");
        dashboardItem.setAmountType("Disponible");
        list.add(dashboardItem);
        dashboardItem = new DashboardItem();
        dashboardItem.setAccountName("Provincial *8731");
        dashboardItem.setAmountType("Disponible");
        dashboardItem.setAccountType("Corriente");
        dashboardItem.setAmount(4500000);
        list.add(dashboardItem);
        dashboardHeader = new DashboardHeader();
        dashboardHeader.setAmount(0);
        dashboardHeader.setHeaderName("Creditos");
        list.add(dashboardHeader);
        /*dashboardItem = new DashboardItem();
        dashboardItem.setAmount(2500000);
        dashboardItem.setAccountType("Ref. 75312");
        dashboardItem.setAmountType("14.500.000,00 Bs.");
        dashboardItem.setAccountName("Pago Movil");
        list.add(dashboardItem);
        dashboardItem = new DashboardItem();
        dashboardItem.setAmount(1500000);
        dashboardItem.setAccountType("Ref. 15912");
        dashboardItem.setAmountType("13.000.000,00 Bs.");
        dashboardItem.setAccountName("Transferencia O/B");
        list.add(dashboardItem);*/
        dashboardHeader = new DashboardHeader();
        dashboardHeader.setAmount(10000000);
        dashboardHeader.setHeaderName("Monedero");
        list.add(dashboardHeader);
        dashboardItem = new DashboardItem();
        dashboardItem.setAmount(2500000);
        dashboardItem.setAccountType("Ref. 75312");
        dashboardItem.setAmountType("10.000.000,00 Bs.");
        dashboardItem.setAccountName("Pago Movil");
        list.add(dashboardItem);
        dashboardItem = new DashboardItem();
        dashboardItem.setAmount(1500000);
        dashboardItem.setAccountType("Ref. 15912");
        dashboardItem.setAmountType("7.500.000,00 Bs.");
        dashboardItem.setAccountName("Transferencia O/B");
        list.add(dashboardItem);
        dashboardListAdapter.notifyDataSetChanged();
    }


}
