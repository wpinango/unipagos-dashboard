package com.bhinary.unipago.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bhinary.unipago.Asynctask;
import com.bhinary.unipago.R;
import com.bhinary.unipago.common.PasswordValidator;
import com.bhinary.unipago.dialog.DialogType;
import com.bhinary.unipago.dialog.GeneralDialog;
import com.bhinary.unipago.interfaces.AsynctaskListener;
import com.bhinary.unipago.models.Response;
import com.bhinary.unipago.models.SignUp;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import jp.wasabeef.blurry.Blurry;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class SignupUserActivity extends AppCompatActivity implements AsynctaskListener{

    private EditText etName, etDni,etEmail, etAddress, etPhone,etPass, etRepeatPass;
    private Button btnSingUp;
    private SignUp signUp;
    private GeneralDialog generalDialog;
    private DialogType dialogType;
    private TextView tvResendEmail;
    private String passCreationHelp = "El password debe contener 8 caracteres, un caracter especial, una letra Mayuscula, minuscula" +
            " y numeros";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_client);
        final View view = findViewById(R.id.signup_layout);
        view.post(new Runnable() {
            @Override
            public void run() {
                Blurry.with(SignupUserActivity.this).radius(20).sampling(2).onto((ViewGroup) view);
            }
        });
        etName = findViewById(R.id.et_name);
        etDni = findViewById(R.id.et_dni);
        etAddress = findViewById(R.id.et_address);
        etEmail = findViewById(R.id.et_email);
        etPass = findViewById(R.id.et_pass_change);
        etRepeatPass = findViewById(R.id.et_repeat_pass_change);
        etPhone = findViewById(R.id.et_phone);
        tvResendEmail = findViewById(R.id.tv_resend_email);
        tvResendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEnterEmailDialog();
            }
        });
        btnSingUp = findViewById(R.id.btn_sign_up);
        btnSingUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    populateDataSingUp();
                }
            }
        });
        generalDialog = new GeneralDialog(this);
        dialogType = new DialogType(this);
        //presentShowcase();
    }

    private void populateDataSingUp() {
        signUp = new SignUp();
        signUp.setIdentityNumber(etDni.getText().toString());
        signUp.setAddress(etAddress.getText().toString());
        signUp.setPassword(etPass.getText().toString());
        signUp.setUserName(etName.getText().toString());
        signUp.setEmail(etEmail.getText().toString());
        signUp.setPhone(etPhone.getText().toString());
        signUp.setStatus(0);
        signUp.setRole(0);
        requestAddUser();
    }

    private void requestAddUser(){
        btnSingUp.setEnabled(false);
        showProgressDialog();
        String toJson = new Gson().toJson(signUp);
        Map<String, String> map = new HashMap<>();
        new Asynctask.PostMethodAsynctask(toJson, Asynctask.URL_ADD_USER, map, this).execute();
    }

    private void presentShowcase() {
        ShowcaseConfig showcaseConfig = new ShowcaseConfig();
        showcaseConfig.setDelay(500);
        MaterialShowcaseSequence materialShowcaseSequence = new MaterialShowcaseSequence(this);
        materialShowcaseSequence.setConfig(showcaseConfig);
        materialShowcaseSequence.addSequenceItem(new MaterialShowcaseView.Builder(this)
                .setTarget(etDni)
                .setDismissText("Entendido")
                .setTitleText("Cedula de identidad")
                .setContentText("Indetifique el documento de identidad si V, J, E")
                .withRectangleShape()
                .build());
        materialShowcaseSequence.addSequenceItem(new MaterialShowcaseView.Builder(this)
                .setTarget(etPhone)
                .setDismissText("Entendido")
                .setContentText("Introduzca su numero telefonico colocando el codigo de pais ejemplo +584142234413")
                .withRectangleShape()
                .build());
        materialShowcaseSequence.addSequenceItem(new MaterialShowcaseView.Builder(this)
                .setContentText(passCreationHelp)
                .setTarget(etPass)
                .setDismissText("Entendido")
                .withRectangleShape()
                .build());
        materialShowcaseSequence.start();
    }

    private boolean validate(){
        boolean valid = true;
        if (etName.getText().toString().isEmpty()){
            etName.setError("Ingrese nombre valid");
            valid = false;
        } else {
            etName.setError(null);
        }
        if (etDni.getText().toString().isEmpty() || etDni.getText().toString().length() < 7){
            etDni.setError("Ingrese cedula valida");
            valid = false;
        } else {
            etDni.setError(null);
        }
        if (etAddress.getText().toString().isEmpty()){
            valid = false;
            etAddress.setError("Ingrese direccion de habitacion valida");
        } else {
            etAddress.setError(null);
        }
        if (etEmail.getText().toString().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()){
            valid = false;
            etEmail.setError("Ingrese correo electronico valida");
        } else {
            etEmail.setError(null);
        }
        if (new PasswordValidator().validate(etPass.getText().toString())){
            etPass.setError(null);
        } else {
            valid = false;
            etPass.setError(passCreationHelp);
        }
        if (etRepeatPass.getText().toString().isEmpty()) {
            valid = false;
            etRepeatPass.setError("Ingrese contaseña valida");
        } else {
            etRepeatPass.setError(null);
        }
        if (!etRepeatPass.getText().toString().equals(etPass.getText().toString())){
            valid = false;
            etRepeatPass.setError("No coincide la contraseña");
        } else {
            etRepeatPass.setError(null);
        }
        if (etPhone.getText().toString().isEmpty() || etPhone.getText().length() != 13) {
            valid = false;
            etPhone.setError(null);
        }
        if (etDni.getText().toString().contains("V") || etDni.getText().toString().contains("E") ||
                etDni.getText().toString().contains("J")) {
            etDni.setError(null);
        } else {
            valid = false;
            etDni.setError("Identifique el documento");
        }
        return valid;
    }

    @Override
    public void onAsynctaskFinished(Enum requestType, String response, String headers) {
        btnSingUp.setEnabled(true);
        generalDialog.sendEmptyMessage(generalDialog.dismissDialog);
        try {
            if (!response.equals("")) {
                Response res = new Gson().fromJson(response, Response.class);
                if (res.getFieldErrors() == null) {
                    if (requestType.equals(AsynctaskListener.requestType.ADDUSER)) {
                        switch (res.getMessage()) {
                            case Asynctask.RESPONSE_OK:
                                clearFields();
                                dialogType.showSuccessDialogWithListenerButton("Se ha registrado correctamente", sweetAlertDialog -> {
                                    this.finish();
                                });
                                showSuccessDialog();
                                //todo ir a login
                                break;
                            case Asynctask.USER_EXISTS:
                                clearFields();
                                showInfoDialog(getResources().getString(R.string.user_exists));
                                break;
                            case Asynctask.EMAIL_EXISTS:
                                clearFields();
                                showInfoDialog(getResources().getString(R.string.email_exist));
                                break;
                        }
                    } else if (requestType.equals(AsynctaskListener.requestType.RESENDEMAIL)) {
                        if (res.getMessage().equals(Asynctask.RESPONSE_OK)) {
                            dialogType.showSuccessDialog("Correo enviado a su direccion de correo");
                        } else if (res.getMessage().equals(Asynctask.NOT_FOUND)) {
                            dialogType.showErrorDialog("No encontrado");
                            showEnterEmailDialog();
                        }
                    }
                } else if (res.getFieldErrors() != null) {
                    if (res.getFieldErrors().get(0).getMessage().equals(Asynctask.IDENTITY_ERROR)) {
                        showErrorDialog(getResources().getString(R.string.error_identity));
                        etDni.setError("Ingrese un numero de identidad valida o identifique el tipo de documento");
                    } else if (res.getFieldErrors().get(0).getMessage().equals(Asynctask.PASSWORD_ERROR)) {
                        showErrorDialog(getResources().getString(R.string.error_password));
                        etPass.setError("Ingrese contraseña valida");
                        etRepeatPass.setError("Ingrese contraseña valida");
                    }
                }
            } else {
                dialogType.showDialogSomethingWrong();
            }
        }catch (Exception e) {
            e.getMessage();
        }
    }

    private void showInfoDialog(String info) {
        Message message = new Message();
        message.what = generalDialog.showInfoDialog;
        message.obj = info;
        generalDialog.sendMessage(message);
    }

    private void showProgressDialog() {
        Message message = new Message();
        message.what = generalDialog.showProgressDialog;
        message.obj = "Registrando usuario ... ";
        generalDialog.sendMessage(message);
    }

    private void showSuccessDialog() {
        Message message = new Message();
        message.what = generalDialog.showSuccesDialog;
        message.obj = "Se ha registrado satisfactioriamente";
        generalDialog.sendMessage(message);
    }

    private void showErrorDialog(String error) {
        Message message = new Message();
        message.what = generalDialog.showErrorDialog;
        message.obj = error;
        generalDialog.sendMessage(message);
    }

    private void clearFields(){
        etRepeatPass.setText("");
        etAddress.setText("");
        etPhone.setText("");
        etEmail.setText("");
        etPass.setText("");
        etPass.setText("");
        etName.setText("");
        etDni.setText("");
    }

    private void showEnterEmailDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_resend_email, null);
        EditText etResendEmail = view.findViewById(R.id.et_resend_email);
        builder.setTitle("Ingrese la direccion de correo");
        builder.setMessage("Para enviar el link de validacion");
        builder.setView(view);
        builder.setPositiveButton("Aceptar", (dialog, which) -> {});
        builder.setNegativeButton("Cancelar",null);
        builder.setCancelable(false);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
            if (etResendEmail.getText().toString().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(
                    etResendEmail.getText().toString()).matches()){
                etResendEmail.setError("Ingrese correo electronico valida");
            } else {
                Map<String, String > headers = new HashMap<>();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("email",etResendEmail.getText().toString());
                new Asynctask.PostMethodAsynctask(jsonObject.toString(),Asynctask.URL_RESEND_EMAIL,
                        headers,this).execute();
                etResendEmail.setError(null);
                alertDialog.dismiss();
            }
        });
    }
}

//status 0: no ha validado el correo
//status 1: no ha configurado la cuenta
//status 2:  ya configurado y validado

