package com.bhinary.unipago.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bhinary.unipago.Asynctask;
import com.bhinary.unipago.Global;
import com.bhinary.unipago.R;
import com.bhinary.unipago.dialog.DialogType;
import com.bhinary.unipago.dialog.GeneralDialog;
import com.bhinary.unipago.interfaces.LoginListener;
import com.bhinary.unipago.models.AccountStatus;
import com.bhinary.unipago.models.Response;
import com.bhinary.unipago.models.UserCredentials;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import jp.wasabeef.blurry.Blurry;

public class LoginActivity  extends AppCompatActivity implements LoginListener{

    private EditText etUser, etPass;
    private Button btnLogin;
    private GeneralDialog generalDialog;
    private DialogType dialogType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etUser = findViewById(R.id.et_email);
        etPass = findViewById(R.id.et_pass_login);
        btnLogin = findViewById(R.id.btn_do_login);
        if (!getUser().equals("")){
            etUser.setText(getUser());
        }
        /*etUser.setText("V20049650");
        etPass.setText("@Cr20598221");*/
        generalDialog = new GeneralDialog(this);
        dialogType = new DialogType(this);
        TextView tvForgotPass = findViewById(R.id.tv_forgot_pass);
        tvForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgotYourPassActivity.class);
                startActivity(intent);
            }
        });
        final View view = findViewById(R.id.layout_login);
        view.post(() -> Blurry.with(LoginActivity.this).radius(15).sampling(2).onto((ViewGroup) view));
        TextView tvSignup = findViewById(R.id.don_t_have_);
        tvSignup.setOnClickListener(view1 -> {
            Intent intent = new Intent(LoginActivity.this,SignupUserActivity.class);
            startActivity(intent);
        });
        btnLogin.setOnClickListener(view12 -> {
            if (validate()) {
                btnLogin.setEnabled(false);
                loginRequest();
            }
        });
    }

    private void saveUser(){
        SharedPreferences preferences = getSharedPreferences("localData", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString("user", etUser.getText().toString());
        edit.apply();
    }

    private String getUser() {
        return getSharedPreferences("localData", Context.MODE_PRIVATE).getString("user","");
    }

    private boolean validate() {
        boolean valid = true;
        if (etPass.getText().toString().isEmpty()) {
            valid = false;
            etPass.setError("Introduzca una contraseña valida");
        } else {
            etPass.setError(null);
        }
        if (etUser.getText().toString().contains("V") || etUser.getText().toString().contains("E") ||
                etUser.getText().toString().contains("J")) {
            etUser.setError(null);
        } else if (etUser.getText().toString().isEmpty()) {
            etUser.setError("Introduzca un nombre de usuario valido");
        }
        else {
            valid = false;
            etUser.setError("Identifique el usuario, V, J o E");
        }
        return valid;
    }

    private void loginRequest() {
        showProgressDialog();
        saveUser();
        new Asynctask.LoginAsynctask(Asynctask.URL_LOGIN,this, etUser.getText().toString(),
                etPass.getText().toString()).execute();
    }

    private void showProgressDialog() {
        Message message = new Message();
        message.what = generalDialog.showProgressDialog;
        message.obj = "Ingresando";
        generalDialog.sendMessage(message);
    }

    private void showErrorDialog() {
        Message message = new Message();
        message.what = generalDialog.showErrorDialog;
        message.obj = "Error al ingresar";
        generalDialog.sendMessage(message);
    }

    private void showInfoDialog(String info) {
        Message message = new Message();
        message.what = generalDialog.showInfoDialog;
        message.obj = info;
        generalDialog.sendMessage(message);
    }

    @Override
    public void onLoginFinish(String response, String headers) {
        generalDialog.sendEmptyMessage(generalDialog.dismissDialog);
        btnLogin.setEnabled(true);
        System.out.println("valores " + response);

        try {
            if (!response.equals("")) {
                Response res = new Gson().fromJson(response, Response.class);
                if (res.getFieldErrors() == null) {
                    if (res.getMessage().equals(Asynctask.RESPONSE_OK)) {
                        UserCredentials userCredentials = new Gson().fromJson(new Gson().toJson(res.getData()), UserCredentials.class);
                        Global.userId = String.valueOf(userCredentials.getId());
                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(headers).getAsJsonObject();
                        Global.token = obj.get(Asynctask.X_SESSION).getAsString();
                        if (!userCredentials.getTemporalPassword()) {
                            if (userCredentials.getStatus() == AccountStatus.EMAIL_NOT_VALIDATED) {
                                showInfoDialog(getResources().getString(R.string.user_not_validated));
                            } else if (userCredentials.getStatus() == AccountStatus.ACCOUNT_NOT_CONFIGURATED) {
                                Intent intent = new Intent(this, AppConfigurationActivity.class);
                                startActivity(intent);
                                this.finish();
                            } else if (userCredentials.getStatus() == AccountStatus.EMAIL_AND_ACCOUNT_VALIDATE) {
                                Intent intent = new Intent(this, MainActivity.class);
                                startActivity(intent);
                                this.finish();
                            }
                        } else {
                            Intent intent = new Intent(LoginActivity.this, ChangePassActivity.class);
                            startActivity(intent);
                        }
                    } else {
                        showErrorDialog();
                    }
                }
            } else {
                dialogType.showDialogSomethingWrong();
            }
        }catch (Exception e) {
            e.getMessage();
        }
    }
}