package com.bhinary.unipago.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bhinary.unipago.Asynctask;
import com.bhinary.unipago.R;
import com.bhinary.unipago.dialog.DialogType;
import com.bhinary.unipago.interfaces.AsynctaskListener;
import com.bhinary.unipago.models.Response;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import jp.wasabeef.blurry.Blurry;

public class ForgotYourPassActivity extends AppCompatActivity implements AsynctaskListener {

    private Button btnSend;
    private EditText etEmail;
    private DialogType dialogType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_your_password);
        final View view = findViewById(R.id.forgot_pass_layout);
        view.post(() -> Blurry.with(ForgotYourPassActivity.this).radius(15).sampling(2)
                .onto((ViewGroup) view));
        TextView tvBack = findViewById(R.id.tv_link_forgot);
        dialogType = new DialogType(this);
        etEmail = findViewById(R.id.et_email);
        btnSend = findViewById(R.id.btn_reset_password);
        btnSend.setOnClickListener(view12 -> {
            if (validate()){
                requestTempPassword();
            }
        });
        tvBack.setOnClickListener(view1 -> ForgotYourPassActivity.this.finish());
    }

    private boolean validate() {
        boolean valid = true;
        if (etEmail.getText().toString().equals("") || !Patterns.EMAIL_ADDRESS.matcher(
                etEmail.getText().toString()).matches()) {
            etEmail.setError("Ingrese un correo valido");
            valid = false;
        } else {
            etEmail.setError(null);
        }
        return valid;
    }

    private void requestTempPassword(){
        Map<String, String> headers = new HashMap<>();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("email",etEmail.getText().toString());
        dialogType.showProgressDialog("Procesando ... ");
        new Asynctask.PostMethodAsynctask(jsonObject.toString(),Asynctask.URL_RESET_PASSWORD,headers,
                this).execute();
    }

    @Override
    public void onAsynctaskFinished(Enum requestType, String response, String headers) {
        dialogType.dismissDialog();
        if (!response.equals("")) {
            Response res = new Gson().fromJson(response,Response.class);
            if (res.getMessage().equals(Asynctask.RESPONSE_OK)){
                dialogType.showSuccessDialog("La clave temporal ha sido enviada a su correo");
            } else if (res.getMessage().equals(Asynctask.EMAIL_NOT_FOUND)) {
                dialogType.showErrorDialog("El email no existe");
            }
        } else {
            dialogType.showDialogSomethingWrong();
        }
    }
}
