package com.bhinary.unipago.dialog;

import android.content.Context;
import android.os.Message;
import android.view.View;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class DialogType {

    private GeneralDialog generalDialog;
    private Context context;
    private SweetAlertDialog sweetAlertDialog;

    public DialogType(Context context) {
        this.generalDialog = new GeneralDialog(context);
        this.context = context;
    }

    public void showProgressDialog(String msg) {
        Message message = new Message();
        message.what = generalDialog.showProgressDialog;
        message.obj = msg;
        generalDialog.sendMessage(message);
    }

    public void showErrorDialog(String error) {
        Message message = new Message();
        message.what = generalDialog.showErrorDialog;
        message.obj = error;
        generalDialog.sendMessage(message);
    }

    public void showInfoDialog(String info) {
        Message message = new Message();
        message.what = generalDialog.showInfoDialog;
        message.obj = info;
        generalDialog.sendMessage(message);
    }

    public void showSuccessDialog(String msg) {
        Message message = new Message();
        message.what = generalDialog.showSuccesDialog;
        message.obj = msg;
        generalDialog.sendMessage(message);
    }

    public void showErrorDialogWithListenerButton(String error, SweetAlertDialog.OnSweetClickListener listener) {
        sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setContentText("Error");
        sweetAlertDialog.setTitle(error);
        sweetAlertDialog.setConfirmButton("Aceptar", listener);
        sweetAlertDialog.show();
    }

    public void showSuccessDialogWithListenerButton(String successMsg, SweetAlertDialog.OnSweetClickListener listener) {
        sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setTitle("Correcto");
        sweetAlertDialog.setContentText(successMsg);
        sweetAlertDialog.setConfirmButton("Aceptar", listener);
        sweetAlertDialog.show();
    }

    public void showInfoDialogWithListenerButton(String infoMsg,SweetAlertDialog.OnSweetClickListener listener) {
        sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
        sweetAlertDialog.setTitle("Informacion");
        sweetAlertDialog.setContentText(infoMsg);
        sweetAlertDialog.setConfirmButton("Aceptar", listener);
        sweetAlertDialog.show();
    }

    public void showInfoDialogWithListenerButtons(String infoMsg,SweetAlertDialog.OnSweetClickListener positiveButton,
                                                  SweetAlertDialog.OnSweetClickListener negativeButton) {
        sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
        sweetAlertDialog.setTitle("Informacion");
        sweetAlertDialog.setContentText(infoMsg);
        sweetAlertDialog.setConfirmButton("Aceptar", positiveButton);
        sweetAlertDialog.setCancelButton("Cancelar", negativeButton);
        sweetAlertDialog.show();
    }

    public void showDialogSomethingWrong(){
        sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitle("Error");
        sweetAlertDialog.setContentText("Algo salio mal");
        sweetAlertDialog.setConfirmButton("Aceptar", null);
        sweetAlertDialog.show();
    }

    public void dismissDialog(){
        generalDialog.sendEmptyMessage(generalDialog.dismissDialog);
        if (sweetAlertDialog != null) {
            sweetAlertDialog.dismiss();
        }
    }

    public void showWarningDeletetionWithListenerButton(String msg, SweetAlertDialog.OnSweetClickListener listener){
        sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        sweetAlertDialog.setTitle("Advertencia");
        sweetAlertDialog.setContentText(msg);
        sweetAlertDialog.setConfirmButton("Aceptar", listener);
        sweetAlertDialog.show();
    }

}
