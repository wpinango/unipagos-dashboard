package com.bhinary.unipago.dialog;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.afollestad.materialdialogs.MaterialDialog;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class GeneralDialog extends Handler {

    public int showProgressDialog = 10;
    public int dismissDialog = 11;
    public int showErrorDialog = 12;
    public int showSuccesDialog = 13;
    public int showInfoDialog = 14;
    private MaterialDialog materialDialog;
    private Context context;
    private SweetAlertDialog sweetAlertDialog;

    public GeneralDialog(Context context){
        this.context = context;
    }

    public void handleMessage(Message msg) {
        if (msg.what == dismissDialog) {
            if (materialDialog != null) {
                materialDialog.dismiss();
            }
            if (sweetAlertDialog != null) {
                sweetAlertDialog.dismiss();
            }
        } else if (msg.what == showProgressDialog) {
            materialDialog = new MaterialDialog.Builder(context).content(msg.obj.toString())
                    .progress(true,0)
                    .cancelable(false)
                    .show();
        } else if (msg.what == showErrorDialog) {
            sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setContentText(msg.obj.toString());
            sweetAlertDialog.setTitle("Algo ha salido mal");
            sweetAlertDialog.show();
        } else if (msg.what == showSuccesDialog) {
            sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
            sweetAlertDialog.setContentText(msg.obj.toString());
            sweetAlertDialog.setTitle("Correcto");
            sweetAlertDialog.show();
        } else if (msg.what == showInfoDialog) {
            sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
            sweetAlertDialog.setContentText(msg.obj.toString());
            sweetAlertDialog.setTitle("Informacion");
            sweetAlertDialog.show();
        }
    }

}
